# Función para imprimir el tablero
def print_board(board):
    print("-------------")
    for i in range(3):
        print("|", board[i][0], "|", board[i][1], "|", board[i][2], "|")
        print("-------------")

# Función para comprobar si se ha producido un ganador
def check_winner(board, player):
    # Comprobar filas
    for i in range(3):
        if board[i][0] == player and board[i][1] == player and board[i][2] == player:
            return True
    # Comprobar columnas
    for i in range(3):
        if board[0][i] == player and board[1][i] == player and board[2][i] == player:
            return True
    # Comprobar diagonales
    if board[0][0] == player and board[1][1] == player and board[2][2] == player:
        return True
    if board[0][2] == player and board[1][1] == player and board[2][0] == player:
        return True
    return False

# Función para jugar el juego
def play_game():
    # Inicializar el tablero
    board = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
    # Inicializar el jugador actual
    current_player = "X"
    # Iniciar el bucle de juego
    while True:
        # Imprimir el tablero actual
        print_board(board)
        # Pedir al jugador que seleccione una fila y una columna
        row = int(input("Ingrese la fila (1, 2, 3) para " + current_player + ": ")) - 1
        col = int(input("Ingrese la columna (1, 2, 3) para " + current_player + ": ")) - 1
        # Comprobar si la selección del jugador es válida
        if board[row][col] == " ":
            board[row][col] = current_player
            # Comprobar si el jugador actual ha ganado
            if check_winner(board, current_player):
                print_board(board)
                print("¡Felicidades! ¡Jugador " + current_player + " ha ganado!")
                return
            # Cambiar al siguiente jugador
            if current_player == "X":
                current_player = "O"
            else:
                current_player = "X"
        else:
            print("Esa casilla ya está ocupada. Intente de nuevo.")

# Llamar a la función para iniciar el juego
play_game()
